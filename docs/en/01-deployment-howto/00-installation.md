# Installation

>**DISCLAIMER**: this is not legal nor technical advice. It is just a WIP proposal, open to discussion, on how a Jitsi-Meet server can be quickly (and legally) deployed in order to host one's own private videomeetings, based on authors' practical experiences.

## Server requirements

- Server: should work on 1 core, 1GB RAM - suggested (at least) 2 core, 2GB RAM (scale at your needs)
- OS: debian 10 minimal (just sshd and base system utilities) - ubuntu 18.04
- hostname: `meet.example.com` (change it to the actual domain you are using)
- create DNS A records for `meet.example.com`.

> NOTE: you do not need to create a DNS record for auth.meet.example.com. It is just [an internal domain used by jicofo to connect to prosody](https://github.com/jitsi/jicofo/blob/master/README.md#manual-prosody-configuration). 

## Basic system configuration

### Security

&rarr; first of all, create a new user, add it to sudoers group and logout as root:

```shell
apt install -y sudo
adduser USER_NAME_OF_YOUR_CHOICE
usermod -a -G sudo USER_NAME_OF_YOUR_CHOICE
exit
```

login as the newly created user and add your public SSH key(s):

```shell
mkdir ~/.ssh
nano ~/.ssh/authorized_keys
```
save and exit (alternatively, you may use `ssh-copy-id USER_NAME_OF_YOUR_CHOICE@meet.example.com` on your client machine).

&rarr; disable SSH Password login and SSH root login:

```shell
sudo nano /etc/ssh/sshd_config
```
changing `PasswordAuthentication` and `PermitRootLogin` values to `no` (uncomment if necessary), save and exit, then restart sshd to apply changes (your ssh connection will *not* be interrupted):

```shell
sudo service sshd restart
```

<!-- altri settings consigliati: AllowUsers [user] LoginGraceTime 1m, Port [n], ClientAliveInterval 600, ClientAliveCountMax 0 -->

Update your system (mantain local config files if asked) and reboot:

```shell
sudo apt update
sudo apt dist-upgrade -y && sudo reboot
```

&rarr; install fail2ban:

```shell
sudo apt install -y fail2ban
```

Later on you will configure Fail2Ban to work with Prosody (see "Fail2Ban for Prosody" in [authentication how-to](01-authentication.md#fail2ban-for-prosody)).

&rarr; set and enable firewall (UFW):

```shell
sudo apt install -y ufw
sudo ufw allow OpenSSH
sudo ufw allow http
sudo ufw allow https
sudo ufw allow in 10000:20000/udp
sudo ufw allow 5222/tcp
sudo ufw allow 5269/tcp
sudo ufw enable
```

### Setup hostname and FQDN

Set hostname and FQDN values (change `meet` and `meet.example.com` to the actual domain you are using):

```shell
sudo hostnamectl set-hostname meet
sudo sed -i 's/^127.0.1.1.*$/127.0.1.1 meet.example.com meet/g' /etc/hosts
```

then check hostname, it should look like this:

```shell
user@meet:~$ hostname
meet
user@meet:~$ hostname -f
meet.example.com
```

**IMPORTANT** Remember to create a DNS A record for `meet.example.com`. 

## Install prerequisites

You should install nginx **before** jitsi-meet, so that you won't need to manually configure the webserver:  
(this is actually needed only on Ubuntu, but since someone reported having  issues also with Debian, you have better to do it anyway)

```shell
sudo apt install -y nginx
sudo systemctl start nginx.service
sudo systemctl enable nginx.service
```

Jitsi is reported not to work with OpenJDK 10: many recommend using OpenJDK 8, but OpenJDK 11 appears to be OK.  
You should check which Java version is automatically installed along with jitsi-meet while [installing it](#install-jitsi-meet).  

If you want to stick to OpenJDK 8, do it **before** installing jitsi-meet:

```shell
sudo apt install -y openjdk-8-jre-headless
```

## Install Jitsi-meet

Add Jitsi repository and install jitsi-meet (remember to check OpenJDK version - do not add `-y` to `apt` command):

```shell
wget -qO -  https://download.jitsi.org/jitsi-key.gpg.key | sudo apt-key add -
sudo sh -c "echo 'deb https://download.jitsi.org stable/' > /etc/apt/sources.list.d/jitsi-stable.list"
sudo apt update -y
sudo apt install jitsi-meet
```

when requested, enter your `meet.example.com` as FQDN and choose to generate a self signed certificate.

## Install Let's Encrypt Certificates

Run auto-install script:
```
sudo /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh
```

when requested, enter your email (to receive certificate renewal notifications -- **IMPORTANT**: put an email address that you actually use, such [notifications are important](#important-certificates-renewal)!) and wait for the process to be successfully completed (in case of failure, relaunch the script above).

Now you have a jitsi-meet server up and running at https://meet.example.com (without any authentication: anyone can enter, create rooms and invite people).

## IMPORTANT: Certificates renewal

There is an open issue on github about letsencrypt certificates renewal problems (<https://github.com/jitsi/jitsi-meet/issues/2885>). However, it does *not* appear to apply to the standard debian/ubuntu installation described here. In any case, **if there are problems with certificates renewal, you will be notified by email**; in such case, just run again the [command above](#install-letsencrypt-certificates).

*NOTE**: The command above does not create the certificate for Prosody (required to allow password change through external XMPP clients), so you have to import it manually (see the how-to page on [authentication](01-authentication.md)).
