# jitsi-self-hosting

>**DISCLAIMER**: this is not legal nor technical advice. It is just a WIP proposal, open to discussion, on how a Jitsi server can be quickly (and legally) deployed in order to host one's own private videomeetings -- "one" meaning a teacher, a school, a professional, a business providing videomeeting services for their own students, colleagues or clients, in the spirit of user-owned cloud. 

This project by the Jitsi Club includes **documentation and scripts to fill gaps** and put together some sparse pieces of the official documentation, in order to enable people to **implement a self-hosted videoconferencing solution that can compete with mainstream proprietary solutions and that is privacy compliant, too**. 

## The Problems and the Solution

On the one hand, the **default Jitsi deployment** (official debian packages) is intended for supplying a videoconferencing system open to anyone in the Internet, without registration.
Since Jitsi

 - *does* process (and store in logs) browsing data and conversation metadata and
  
 - *does not* implement E2E encryption (unless under certain specific use conditions) -- so audio/video streams flow unencrypted within the server,

offering a Jitsi server accessible to anyone without authentication may imply **legal and security issues that cannot be handled (and it would not make sense they were handled) by small organizations** and individuals, and in any case **it does not fit the needs** of who is willing to **set up a private videoconferencing system to host their own private meetings**. Moreover, some Jitsi default audio/video settings are not optimal for hosting meetings with more than 10 participants. So, **installing Jitsi is easy, but its practical use requires manual setting**.

On the other hand, Jitsi is made of different sub-components and some of them (Prosody) are third party components, thus **the relevant documentation is fragmented** accordingly; one has to search and put together different pieces of documentation from different sources to achieve the desired result (set up a private videoconferencing system to host one's own private meetings). Moreover, **as to privacy compliance concerns, the relevant information to make informed decisions as Data Controller** (in the sense of the GDPR) **is fragmented, too, and in some cases it is rather imprecise**.

So we constituted the "Jitsi Club" and created this project to share experiences among self-hosting Jitsi users and **define a standard deployment for a private, self-hosted, GDPR-compliant videoconferencing system**. 

## Covered Topics

- legal compliance analysis and compliant implementation proposals; 
 
- **things to know about Jitsi in order to deploy it in a GDPR compliant way** (in brief: even if Jitsi does not store conversation contents by default, it *does* process personal data to operate and store browsing data and conversation metadata in logs, which *are* personal data; it implements client-server encryption, but it does *not* generally implement end-to-end encryption between clients, unless in some cases; authentication is *not* enabled by default and Jitsi is accessible by anyone in the Internet - so it has to be properly configured, in order to be GDPR compliant);

- simple guide on **how to implement Jitsi in a GDPR-compliant way** and to **optimize it for use by medium and large teams/groups**, covering some **topics that are not covered in Jitsi official documentation**;

- **TODO**: privacy policy template;

- **TODO**: scripts to fully automate the installation, configuration and tuning described in the guide.
